let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'], kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log('Pikachu! I choose you!');
	}
};

console.log(trainer);

console.log('Result of dot notation:');
console.log(trainer.name);

console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

console.log('Result of talk method');
trainer.talk();
class Pokemon {
	constructor (name, level) {
		// properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// methods
		this.tackle = function(target){
			let health = Number(target.health - this.attack);
			target.health = health;

			console.log(this.name + ' tackled ' + target.name);
			console.log(target.name + "'s health is now reduced to " + health);

			if (health <= 0){
				console.log(target.name + " fainted.");
			}
		}
	}
}

let pikachu = new Pokemon('Pikachu', 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);